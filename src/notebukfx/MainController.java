/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package notebukfx;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author apiep
 */
public class MainController implements Initializable {

    private Database db;
    @FXML
    private TextField textTitle;
    @FXML
    private TextArea textContent;
    @FXML
    private ListView noteList;
    public MainController() {
        db = new Database();
        noteList = new ListView();
    }
    @FXML
    public void btnAddClicked(ActionEvent event) {
        textTitle.setText("");
        textContent.setText("");
        textTitle.requestFocus();
    }
    @FXML
    public void btnSaveClicked(ActionEvent event) {
        int index = noteList.getItems().size() + 1;
        System.out.println(index);
        String title = textTitle.getText();
        String content = textContent.getText();
        Note note = new Note(title, content);
        noteList.getItems().add(note);
        db.addNewData(index, title, content);
    }
    @FXML
    public void btnUpdateClicked(ActionEvent event) {
        Note note = (Note) noteList.getSelectionModel().getSelectedItem();
        note.setTitle(textTitle.getText());
        note.setContent(textContent.getText());
        db.updateData(note.getIndex(), note.getTitle(), note.getContent());
    }
    @FXML
    public void btnGetClicked(ActionEvent event){
        noteList.setItems(db.getNote());
    }
    @FXML
    public void noteListClicked(MouseEvent event) {
        textTitle.setText(((Note) noteList.getSelectionModel().getSelectedItem()).getTitle());
        textContent.setText(((Note) noteList.getSelectionModel().getSelectedItem()).getContent());
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
