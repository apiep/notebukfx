package notebukfx;

import java.io.Serializable;

public class Note implements Serializable {
    private int index;
    private String title;
    private String content;
    public Note(){}
    public Note(int index, String title, String content){
        this.index = index;
        this.title = title;
        this.content = content;
    }
    public Note(String title, String content) {
        this.title = title;
        this.content = content;
    }
    public int getIndex(){return index;}
    public String getTitle(){return title;}
    public String getContent(){return content;}
    public void setIndex(int index){this.index = index;}
    public void setTitle(String title){this.title = title;}
    public void setContent(String content){this.content = content;}
    @Override
    public String toString(){return this.title;}
}