/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package notebukfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author apiep
 */
public class NotebukFX extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));
        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(getClass().getResource("style.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("NotebukFX");
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
