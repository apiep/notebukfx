package notebukfx;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.sqlite.SQLiteConfig;

public class Database {

    private Connection conn;
    private PreparedStatement pst;
    private Statement st;
    private ResultSet rs;
    private Configuration conf;
    private String urlDb = "jdbc:sqlite:/";
    public Database() {
        conf = new Configuration();
        org.sqlite.SQLiteJDBCLoader.initialize();
        try {
            conn = new SQLiteConfig().createConnection(urlDb + conf.getDbPath());
            init_db();
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void init_db() throws SQLException {
        st = conn.createStatement();
        st.execute("CREATE TABLE IF NOT EXISTS data( "
                + "  id integer primary key autoincrement,"
                + "  index_isi integer,"
                + "  title text,"
                + "  content text"
                + ");");
    }
    public void addNewData(int index, String title, String content) {
        try {
            pst = conn.prepareStatement("INSERT INTO data (index_isi,title,content) VALUES(?,?,?);");
            pst.setInt(1, index);
            pst.setString(2, title);
            pst.setString(3, content);
            pst.executeUpdate();
        }catch(SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String getTitle(int index){
        String hasil = "";
        try{
            pst = conn.prepareStatement("SELECT title FROM data WHERE index_isi == ?");
            pst.setInt(1, index);
            rs = pst.executeQuery();
            while(rs.next()){
                hasil = rs.getString(1);
            }
        }catch(SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null,ex);
        }
        return hasil;
    }
    public String getContent(int index){
        String hasil = "";
        try{
            pst = conn.prepareStatement("SELECT content FROM data WHERE index_isi == ?");
            pst.setInt(1, index);
            rs = pst.executeQuery();
            while(rs.next()){
                hasil = rs.getString(1);
            }
        }catch(SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }
    public void updateData(int index, String title, String content){
        try{
            pst = conn.prepareStatement("UPDATE data SET title=?,content=? WHERE index_isi == ?");
            pst.setString(1, title);
            pst.setString(2, content);
            pst.setInt(3, index);
            pst.executeUpdate();
        }catch(SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    public ObservableList<Note> getNote(){
        ObservableList<Note> list = FXCollections.observableArrayList();
        try{
            st = conn.createStatement();
            rs = st.executeQuery("SELECT index_isi,title,content FROM data;");
            while(rs.next()){
                list.add(new Note(rs.getInt(1), rs.getString(2), rs.getString(3)));
            }
        }catch(SQLException ex){
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
