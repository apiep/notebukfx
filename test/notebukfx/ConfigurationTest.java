/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package notebukfx;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apiep
 */
public class ConfigurationTest {
    
    public ConfigurationTest() {
    }
    @BeforeClass
    public static void setUpClass() {
    }
    @AfterClass
    public static void tearDownClass() {
    }
    @Before
    public void setUp() {
    }
    @After
    public void tearDown() {
    }
    /**
     * Test of setDbPath method, of class Configuration.
     */
    @Test
    public void testSetDbPath() {
        System.out.println("setDbPath");
        String path = "/home/apiep/database_test.db";
        Configuration instance = new Configuration();
        instance.setDbPath(path);
        // TODO review the generated test code and remove the default call to fail.
    }
    /**
     * Test of getDbPath method, of class Configuration.
     */
    @Test
    public void testGetDbPath() {
        System.out.println("getDbPath");
        Configuration instance = new Configuration();
        String expResult = "/home/apiep/database_test.db";
        String result = instance.getDbPath();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
