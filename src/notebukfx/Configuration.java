package notebukfx;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Configuration {

    private JSONObject jSONObject;
    private JSONParser jSONParser;
    private File file;
    public Configuration() {
        jSONObject = new JSONObject();
        jSONParser = new JSONParser();
        file = new File("config.json");
    }
    private FileWriter getFileToWriter() throws IOException {
        return new FileWriter(file);
    }
    private FileReader getFileToRead() throws IOException {
        return new FileReader(file);
    }
    public void setDbPath(String path) {
        jSONObject.put("dbPath", path);
        try {
            FileWriter writer = getFileToWriter();
            writer.write(jSONObject.toJSONString());
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String getDbPath() {
        JSONObject obj = new JSONObject();
        try {
            obj = (JSONObject)jSONParser.parse(getFileToRead());
        } catch (IOException | ParseException ex) {
            Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (String) obj.get("dbPath");
    }
}
